FROM python:3

ADD . /opt/

RUN apt-get update
RUN pip install happybase docker-compose enums requests pyyaml retrying
RUN pip install -U pytest

ENTRYPOINT ["pytest", "/opt/"]
