import pytest
import yaml
import os

from faker import Faker
from domain.azkaban import AzkabanClient
from domain.hbase import HBaseClient
from domain.db import CMDBConnection
from domain.config_init import get_config
from domain.tracker import Tracker
from domain.cm_api import CMClient

from hdfs import InsecureClient

c = get_config()

@pytest.fixture(scope="module")
def api_client(request):
    return CMClient(c)

@pytest.fixture(scope="module")
def azkaban_client(request):
    return AzkabanClient(c)

@pytest.fixture(scope="function")
def hbase_client(request):
    return HBaseClient(c)

@pytest.fixture(scope="function")
def cm_db_client(request):
    return CMDBConnection(pytest.config)

@pytest.fixture(scope="function")
def hdfs_client(request):
    return InsecureClient("http://%s:%d" % (c['webhdfs']['host'], c['webhdfs']['port']), user="hdfs")

@pytest.fixture(scope="function")
def fake(request):
    return Faker()

@pytest.fixture(scope="module")
def tracker(request):
    return Tracker(c)


# class Segmentation():
#     def __init__(self, options):
#         self.options = options
#         self.options['SERVICE'] = ['segmentation']
#         self.cmd = compose_services(options)
#
#     def run(self):
#         self.cmd.up(options)
#         container_is_running = True
#
#         while container_is_running == True:
#             containers = self.cmd.project.containers(service_names=options['SERVICE'], stopped=True)
#             time.sleep(0.5)
#             for container in containers:
#                 if "Exit" in container.human_readable_state:
#                     container_is_running = False
#
#     def fin(self):
#         self.cmd.logs(options)
#
#

# options = {
#     "--no-deps": False,
#     "--abort-on-container-exit": False,
#     "SERVICE": "",
#     "--remove-orphans": False,
#     "--no-recreate": True,
#     "--force-recreate": False,
#     "--build": False,
#     '--no-build': False,
#     '--no-color': False,
#     "--rmi": "none",
#     "--volumes": "",
#     "--follow": False,
#     "--timestamps": False,
#     "--tail": "all",
#     "--scale": "",
#     "-d": True,
# }
#
# def compose_services(options):
#     project = project_from_options(os.path.dirname(__file__), options)
#     cmd = TopLevelCommand(project)
#
#     return cmd
#
#
# @pytest.fixture(scope="session", autouse=True)
# def docker_compose_all(request):
#     options['SERVICE'] = ["zookeeper", "kafka", "hbase", "postgresql", "tracker", "exporter-hbase"]
#     cmd = compose_services(options)
#
#     cmd.up(options)
#     time.sleep(20)
#
#     def docker_finalize():
#         cmd.logs(options)
#         cmd.down(options)
#
#     request.addfinalizer(docker_finalize)
#
# @pytest.fixture(scope="function")
# def segmentation(request):
#     s = Segmentation(options)
#     yield s
#     s.fin()
#
# @pytest.fixture(scope="function")
# def hbase(request, config):
#     h = HBase(pytest.config['hbase'])
#     h.init_tables()
#     yield h
#     h.delete_all_tables()
#
# @pytest.fixture(scope="function")
# def tracker(request, config):
#     t = Tracker(pytest.config['tracker'])
#     yield t
#
