import happybase

connection = happybase.Connection('hbase', 9090)

table_name = "profiles"
try:
    connection.create_table(table_name, families= \
        {
            'HISTORY': dict(max_versions=1000, bloom_filter_type=None, block_cache_enabled=False),
            'PII': dict(max_versions=1000, bloom_filter_type=None, block_cache_enabled=False),
            'INFO': dict(max_versions=1, bloom_filter_type=None, block_cache_enabled=False),
            'SEGMENT': dict(max_versions=1, bloom_filter_type=None, block_cache_enabled=False)
        })
except:
    print("Table already exists")
