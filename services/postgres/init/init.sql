DROP TABLE IF EXISTS SEG_Rule;
DROP TABLE IF EXISTS SEG_RuleType;


CREATE TABLE SEG_RuleType (
  Id bigint NOT NULL,
  ProcessorId bigint NOT NULL,
  Description varchar(512) NOT NULL
);

INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  1,3635879284995168875,'Regular expression for the whole profile data');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  2,8190220943169147894,'Regular expression for Visit url history');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  3,2204676703791783585,'Regular expression for Impressions history');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  4,732452277606823966,'Regular expression for Conversions history');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  5,5896274001140730769,'Regular expression for Clicks history');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  6,432532634729,'Processor for boolean logic combination of other tags');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  7,5736131860338142156,'Processor for custom segment tags');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  9,1928565324194567492,'Processor for custom query over profiles attributes');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  10,3526573967392464835,'Processor for look-alike segment');
INSERT INTO SEG_RuleType (Id,ProcessorId,Description) VALUES (
  11,528540646799,'Multi-action processor');

CREATE TABLE SEG_Rule (
  Id bigint NOT NULL,
  SiteId bigint,
  Name varchar(10485760),
  RuleTypeId bigint NOT NULL,
  Description varchar(10485760),
  OptInValue varchar(10485760) NOT NULL,
  OptOutValue varchar(10485760),
  CreateDate timestamp,
  OwnerId bigint,
  EventTimeBound bigint,
  Ttl bigint,
  LastModifiedDate timestamp
);

INSERT INTO SEG_Rule
(Id,
 SiteId,
 Name,
 RuleTypeId,
 Description,
 OptInValue,
 OptOutValue,
 CreateDate,
 OwnerId,
 EventTimeBound,
 Ttl,
 LastModifiedDate)
VALUES (40529,
        NULL,
        '["mat"]',
        11,
        '[{"desc":"","urlId":"10495"}]',
        '[{"actionTypeId":1,"actions":1,"days":0,"params":{"campaignid":"100","adgroupid":"\\d\\d"}}]',
        '',
        TO_DATE('2017-06-13 17:24:21','YYYY-MM-DD HH24:MI:SS'),
        21628,
        NULL,
        7776000000,
        TO_DATE('2017-06-13 17:24:21','YYYY-MM-DD HH24:MI:SS'));

INSERT INTO SEG_Rule
(Id,
 SiteId,
 Name,
 RuleTypeId,
 Description,
 OptInValue,
 OptOutValue,
 CreateDate,
 OwnerId,
 EventTimeBound,
 Ttl,
 LastModifiedDate)
VALUES (40530,
        NULL,
        '["mat 2"]',
        11,
        '[{"desc":""}]',
        '[{"actionTypeId":1,"actions":1,"days":0,"params":{"advertiserId":"1000"}}]',
        '',
        TO_DATE('2017-07-24 15:07:21','YYYY-MM-DD HH24:MI:SS'),
        21628,
        NULL,
        7776000000,
        TO_DATE('2017-07-24 15:07:21','YYYY-MM-DD HH24:MI:SS'));

INSERT INTO SEG_Rule
(Id,
 SiteId,
 Name,
 RuleTypeId,
 Description,
 OptInValue,
 OptOutValue,
 CreateDate,
 OwnerId,
 EventTimeBound,
 Ttl,
 LastModifiedDate)
VALUES (40531,
        NULL,
        '["boolean"]',
        6,
        '[{"desc":""}]',
        '73 OR 74',
        '',
        TO_DATE('2017-07-24 15:13:21','YYYY-MM-DD HH24:MI:SS'),
        21628,
        NULL,
        7776000000,
        TO_DATE('2017-07-24 15:13:21','YYYY-MM-DD HH24:MI:SS'));

INSERT INTO SEG_Rule
(Id,
 SiteId,
 Name,
 RuleTypeId,
 Description,
 OptInValue,
 OptOutValue,
 CreateDate,
 OwnerId,
 EventTimeBound,
 Ttl,
 LastModifiedDate)
VALUES (40532,
        NULL,
        '["boolean"]',
        6,
        '[{"desc":""}]',
        '(73 OR 74) AND NOT 75',
        '',
        TO_DATE('2017-07-24 15:13:21','YYYY-MM-DD HH24:MI:SS'),
        21628,
        NULL,
        7776000000,
        TO_DATE('2017-07-24 15:13:21','YYYY-MM-DD HH24:MI:SS'));


CREATE TABLE SEG_CustomJar (
  Id bigint NOT NULL,
  RuleId bigint,
  Jar bytea,
  fileName varchar(255),
  CONSTRAINT PK__SEG_Cust__3214EC07931D62C9 PRIMARY KEY (Id)
);


CREATE TABLE SEG_Segment (
	Id bigint NOT NULL,
	Name varchar(192) NOT NULL,
	AdvertiserId bigint,
	AssociationId bigint,
	ProductLineId bigint,
	ProjectId bigint,
	BiddingFee float,
	PlatformFee float,
	RoyaltyFee float,
	CostPerMile float,
	Confidence varchar(128),
	ThirdPartyDataId varchar(128),
	LastModifiedDate timestamp,
	SourceId bigint NOT NULL,
	PercentOfMediaCost float,
	ExternalCostPerMile float,
	ExternalPercentOfMediaCost float,
	PercentOfServingCost float,
	PercentOfSaleRevenue float,
	Ttl bigint,
	Status bigint NOT NULL DEFAULT 3,
	FullPath varchar(512),
	Description varchar(10485760),
	isThirdParty bit
);

INSERT INTO SEG_Segment (Id, Name, AdvertiserId, SourceId) VALUES (2, 'MAT', 1, 1);
INSERT INTO SEG_Segment (Id, Name, AdvertiserId, SourceId) VALUES (3, 'Email', 1, 1);
INSERT INTO SEG_Segment (Id, Name, AdvertiserId, SourceId) VALUES (4, 'Boolean', 1, 1);
INSERT INTO SEG_Segment (Id, Name, AdvertiserId, SourceId) VALUES (5, 'Boolean 2', 1, 1);


CREATE TABLE SEG_SegmentRule (
  Id bigint NOT NULL,
  SegmentId bigint NOT NULL,
  RuleId bigint NOT NULL
);

INSERT INTO SEG_SegmentRule (Id, SegmentId, RuleId) VALUES(2, 2, 40529);
INSERT INTO SEG_SegmentRule (Id, SegmentId, RuleId) VALUES (3, 3, 40530);
INSERT INTO SEG_SegmentRule (Id, SegmentId, RuleId) VALUES (4, 4, 40531);
INSERT INTO SEG_SegmentRule (Id, SegmentId, RuleId) VALUES (4, 5, 40532);
