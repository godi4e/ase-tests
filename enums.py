from enum import Enum

class EventTypes(Enum):
    EMAIL_CLICK = 37
    EMAIL_OPEN = 36
    IMPRESSION = 1
    CLICK = 2
    WEB_VISIT = 16

class Actions(Enum):
    SIMPLIFI_IMPRESSION = 38475681
    SIMPLIFI_CLICK = 38475686
    WEB_VISIT = 38477857

class UserAgent(Enum):
    IPHONE_CHROME = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1'
    ANDROID_CHROME = 'Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>'
    IPHONE_SAFARI = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1'
    DESKTOP_SAFARI = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12'
    IPAD_FIREFOX = 'Mozilla/5.0 (iPad; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) FxiOS/1.0 Mobile/12F69 Safari/600.1.4'
    IPHONE_FIREFOX = '	Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) FxiOS/1.0 Mobile/12F69 Safari/600.1.4'

class Device(Enum):
    DESKTOPS = 2
    TABLETS = 5
    MOBILE = 4

class ZipCode(Enum):
    OMSK = 644041
    US = 12345

class OS(Enum):
    ANDROID = 'Android'
    SAFARI = 'OSX'

class DeviceType(Enum):
    PHONE = 'Phone'
    PC = 'PC'
    MOBILE = 'Mobile'

class BrowserName(Enum):
    CHROME = 'Chrome'
    SAFARI = 'Safari'

class BrowserVersion(Enum):
    CHROME = 0
    SAFARI = 8







