from domain.models.rule import Rule
from domain.models.site import Site
from domain.user_profile import User

import time
from domain.models.segment import Segment


def test_ase_happy_path(azkaban_client):
    advertiserId = 1184
    site_url = "http://TestASE6666.com"
    site = Site(advertiserId, site_url)
    site_id = site.get_site_id()
    print(site_id)
    url_id = site.get_url_id(site_id)
    print(url_id)
    rule = Rule(advertiserId, site_id, url_id)
    rule_id = rule.get_rule_id()
    print(rule_id)
    segment = Segment(advertiserId)
    segment_id = segment.get_segment_id()
    segment.create_segment_with_rule(advertiserId, rule_id, segment_id)
    user1 = User()
    # landing_page = 'http://ep-cm-dev.thumbtack.lo:7070/sync?round=1'
    user1.send_event_to_tracker(
        event_type_id=16,
        referer=site_url)

    time.sleep(10)

    azkaban_client.execute_flow('Epiphany-ASE', 'audience-segmentation')

    # azkaban_client.execute_flow('Epiphany-ASE', 'adserver-segments-exporter')


    count = segment.get_user_count()

    print(count)

    assert count == 1
    # segment.delete_segment(segment_id)
    # conn = CMDBConnection(DBConnection)
    # segment.delete_segment(segment_id)
