from domain.user_profile import User
from domain.models.campaign_group import CampaignGroup

import time

from enums import Actions
from enums import EventTypes

def test_happy_path(azkaban_client):
    campaign_group = CampaignGroup(from_campaign_group=4122)
    campaign_group.set_conversion_settings()
    cg_id = campaign_group.get_campaign_group_id()

    user1 = User()

    user1.send_event_to_tracker(event_type_id=EventTypes['IMPRESSION'].value,
                                type=Actions['SIMPLIFI_IMPRESSION'].value,
                                days_ago=3,
                                campaign_id=cg_id)

    user1.send_event_to_tracker(event_type_id=EventTypes['CLICK'].value,
                                type=Actions['SIMPLIFI_CLICK'].value,
                                days_ago=2,
                                campaign_id=cg_id)

    user1.send_event_to_tracker(event_type_id=EventTypes['WEB_VISIT'].value,
                                type=Actions['WEB_VISIT'].value,
                                days_ago=1,
                                campaign_id=cg_id)

    time.sleep(10)

    azkaban_client.execute_flow('audience-segmentation', 'conversion-path-export')

    r = campaign_group.get_conversion_report(cg_id)

    assert r[0]['path'] == 'Impression (Simpli.fi) -> Click (Simpli.fi) -> Vizit'


def test_no_chain_when_conversion_only(azkaban_client):
    campaign_group = CampaignGroup(from_campaign_group=4122)
    campaign_group.set_conversion_settings()
    cg_id = campaign_group.get_campaign_group_id()

    user1 = User()

    user1.send_event_to_tracker(event_type_id=EventTypes['WEB_VISIT'].value,
                                type=Actions['WEB_VISIT'].value,
                                days_ago=1,
                                campaign_id=cg_id)

    time.sleep(10)

    azkaban_client.execute_flow('audience-segmentation', 'conversion-path-export')

    r = campaign_group.get_conversion_report(cg_id)

    assert r == []

def test_no_chain_when_AI_only(azkaban_client):
    campaign_group = CampaignGroup(from_campaign_group=4122)
    campaign_group.set_conversion_settings()
    cg_id = campaign_group.get_campaign_group_id()

    user1 = User()

    user1.send_event_to_tracker(event_type_id=EventTypes['IMPRESSION'].value,
                                type=Actions['SIMPLIFI_IMPRESSION'].value,
                                days_ago=1,
                                campaign_id=cg_id)

    time.sleep(10)

    azkaban_client.execute_flow('audience-segmentation', 'conversion-path-export')

    r = campaign_group.get_conversion_report(cg_id)

    assert r == []