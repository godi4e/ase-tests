import pytest
import enums

from domain.tracker import Event
from hashlib import md5

user1 = 'test_user1'.encode('utf-8')

def test_multiaction_segmentation(segmentation, hbase, tracker):
    e = Event({
        'userId': 'test_user1',
        'event_type_id':1,
        'landingPage': 'http://google.com',
        'email': 'testing@thumbtack.net',
        'action_id': 1,
        'campaignid': 100,
        'adgroupid': 99
    })

    r = e.send_to(tracker)

    segmentation.run()

    user_data = hbase.get_row_data(md5(user1).hexdigest())
    user_segment_data = user_data.get_segment_data_by_id('40529')

    print(user_segment_data)

    assert user_segment_data['owner_id'] == '21628'
    assert user_segment_data['segment_id'] == '2'
    assert user_segment_data['rule_id'] == '40529'
    assert 'mat' in user_segment_data['rule_name']