import time
from domain.models.deal import Deal

def test_sync():
    deal = Deal()

    deal_cm_db = deal.get_deal_from_cm_db()
    deal_ssp_db = deal.get_deal_from_ssp_db()

    assert deal_cm_db.devices == deal_ssp_db.devices
    assert deal_cm_db.name == deal_ssp_db.name
    assert deal_cm_db.deal_id == deal_ssp_db.id
    assert deal_cm_db.bidders == deal_ssp_db.bidders
    assert deal_cm_db.publishers == deal_ssp_db.publishers
    assert deal_cm_db.landing_page == deal_ssp_db.landing_page
    assert deal_cm_db.geo == deal_ssp_db.geo
    assert deal_cm_db.size == deal_ssp_db.size
    assert deal_cm_db.start_date == deal_ssp_db.start_date
    assert deal_cm_db.end_date == deal_ssp_db.end_date