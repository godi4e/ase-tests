import json
import enums
import pytest
import uuid

from domain.tracker import *
from domain.hbase import HBase


def test_tracker_should_redirect_when_handle_click_events(config, hbase, tracker):
    landing_page = 'http://thumbtack.net'

    e = Event({
        'userId': '3213123213',
        'event_type_id':enums.EventTypes.EMAIL_CLICK.value,
        'landingPage': landing_page, 
        'campaignId': 3392,
        'email': 'testing@thumbtack.net',
        'action_id': 38475692,
        'advertiserId': 24
    })

    r = e.send_to(tracker)

    row_key = hbase.get_row_key_by_user_id(r.user_id)
    row_data = hbase.get_row_data(row_key)

    assert r.response.history[0].headers['Location'] == landing_page
    assert r.response.history[0].status_code == 302

def test_tracker_should_able_to_capture_and_save_email_pii_data(config, tracker, hbase):
    email = 'rkiriluk@thumbtack.net'
    advertiser_id = '3'

    e = Event({
        'event_type_id': enums.EventTypes.EMAIL_OPEN.value,
        'email': email,
        'advertiserId': advertiser_id
    })

    r = e.send_to(tracker)

    row_key = hbase.get_row_key_by_user_id(r.user_id)
    row_data = hbase.get_row_data(row_key)

    assert row_data.get_email() == email
    assert row_data.get_advertiser_id() == advertiser_id

def test_tracker_should_assign_user_id_if_passed_with_query_params(config, tracker, hbase):
    user_id = str(uuid.uuid4())

    e = Event({
        'event_type_id': enums.EventTypes.EMAIL_OPEN.value,
	    'userId': user_id
    })

    r = e.send_to(tracker)

    row_key = hbase.get_row_key_by_user_id(user_id)
    row_data = hbase.get_row_data(row_key)

    assert r.user_id == user_id
