from domain.factories.site import Site
from domain.factories.advertiser import Advertiser
from domain.factories.segment import Segment
from domain.factories.rule import Rule


def test_object_creation(api_client):
    advertiser = Advertiser()
    api_client.advertiser_create(advertiser)

    site = Site(advertiser_id=advertiser.body.id,
                urls='http://google.com')

    api_client.site_create(site)

    print(advertiser.name, advertiser.body.name)

    rule = Rule(url_id=site.body.urls[0].id,
                site_id=site.body.id,
                advertiser_id=advertiser.body.id)

    api_client.rule_create(rule)

    segment = Segment(advertiser_id=advertiser.body.id)

    api_client.segment_create(advertiser_id=advertiser.body.id,
                              segment=segment)

    api_client.bind_rule_to_segment(advertiser_id=advertiser.body.id,
                                    segment_id=segment.body['id'],
                                    rule_id=rule.body['id'])