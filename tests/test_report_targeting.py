from domain.models.ad_group_metric_aggregate import AdGroupMetricAggregate
from domain.models.advertiser_metric_aggregate import AdvertiserMetricAggregate
from domain.models.browser_metric_aggregate import *
from domain.models.creatives import *
from domain.models.device_metric_aggregate import DeviceMetricAggregate
from domain.models.location_metric_aggregate import *
from domain.user_profile import User
from domain.factories.campaign_group import CampaignGroup
from domain.factories.advertiser import Advertiser
from domain.factories.campaign import SimplifiCampaign
from domain.factories.adgroup import SimplifiGroup
from domain.factories.creative import SimplifiCreative
from enums import *
import time
import pytest

city = 'Omsk'
country = 'Russia'
region = '55'

@pytest.fixture(scope='module', autouse=True)
def init_data(azkaban_client, tracker, api_client):
    advertiser = Advertiser()

    api_client.advertiser_create(advertiser)

    campaign_group = CampaignGroup(advertiser_id=advertiser.body.id)
    api_client.campaign_group_create(campaign_group)

    campaign = SimplifiCampaign(advertiser_id=advertiser.body.id, campaign_group_name=campaign_group.body.name)
    api_client.campaign_create(campaign)

    adgroup = SimplifiGroup(campaign_id=campaign.body.id)
    api_client.adgroup_create(adgroup)

    creative = SimplifiCreative(adgroup_id=adgroup.body.id)
    api_client.creative_create(creative)

    advertiser_id = advertiser.body.id
    campaign_group_id = campaign_group.body.id
    campaign_id = campaign.body.id
    ad_group_id = adgroup.body.id
    creative_id = creative.body.id

    user1 = User()
    user1.send_event_to_tracker(type=Actions['SIMPLIFI_IMPRESSION'].value,
                                event_type_id=EventTypes['IMPRESSION'].value,
                                user_agent=UserAgent['ANDROID_CHROME'].value,
                                zip=ZipCode['OMSK'].value,
                                creative_id=creative_id,
                                group_id=ad_group_id,
                                campaign_id=campaign_id,
                                advertiser_id=advertiser_id,
                                city=city,
                                region=region,
                                country=country)

    user1.send_event_to_tracker(type=Actions['SIMPLIFI_IMPRESSION'].value,
                                event_type_id=EventTypes['IMPRESSION'].value,
                                user_agent=UserAgent['ANDROID_CHROME'].value,
                                zip=ZipCode['OMSK'].value,
                                creative_id=creative_id,
                                group_id=ad_group_id,
                                campaign_id=campaign_id,
                                advertiser_id=advertiser_id,
                                city=city,
                                region=region,
                                country=country)

    user1.send_event_to_tracker(type=Actions['SIMPLIFI_IMPRESSION'].value,
                                event_type_id=EventTypes['IMPRESSION'].value,
                                user_agent=UserAgent['ANDROID_CHROME'].value,
                                zip=ZipCode['OMSK'].value,
                                creative_id=creative_id,
                                group_id=ad_group_id,
                                campaign_id=campaign_id,
                                advertiser_id=advertiser_id,
                                city=city,
                                region=region,
                                country=country)

    user1.send_event_to_tracker(type=Actions['SIMPLIFI_CLICK'].value,
                                event_type_id=EventTypes['CLICK'].value,
                                user_agent=UserAgent['DESKTOP_SAFARI'].value,
                                zip=ZipCode['OMSK'].value,
                                creative_id=creative_id,
                                group_id=ad_group_id,
                                campaign_id=campaign_id,
                                advertiser_id=advertiser_id,
                                city=city,
                                region=region,
                                country=country)

    time.sleep(10)

    tracker.rollover()

    azkaban_client.execute_flow('audience-segmentation', 'log-to-sql')
    azkaban_client.execute_flow('audience-segmentation', 'aggregator')
    azkaban_client.execute_flow('audience-segmentation', 'DW_to_MV_export')

    advertiser_statistics = AdvertiserMetricAggregate.get_by_advertiser_id(advertiser_id)

    return {
        'campaign_group_id': campaign_group_id,
        'advertiser_id': advertiser_id,
        'campaign_id': campaign_id,
        'ad_group_id': ad_group_id,
        'creative_id': creative_id,
        'statistic_before': advertiser_statistics
    }

def test_location_metrics_report(init_data):
    location_aggregate = LocationMetricAggregate.get_by_adgroup_id(init_data['ad_group_id'])

    assert location_aggregate.advertiser_id == init_data['advertiser_id']
    assert location_aggregate.ad_group_id == init_data['ad_group_id']
    assert location_aggregate.city == city
    assert location_aggregate.country == country
    assert location_aggregate.region == region
    assert location_aggregate.clicks == 1
    assert location_aggregate.impressions == 3

def test_browser_metrics_report(init_data):
    browser_aggregate = BrowserMetricAggregate.get_by_adgroup_id(init_data['ad_group_id'], EventTypes.IMPRESSION.value)

    assert browser_aggregate.action_id == Actions.SIMPLIFI_IMPRESSION.value
    assert browser_aggregate.browser_version == BrowserVersion.CHROME.value
    assert browser_aggregate.advertiser_id == init_data['advertiser_id']
    assert browser_aggregate.campaign_id == init_data['campaign_id']
    assert browser_aggregate.creative_id == init_data['creative_id']
    assert browser_aggregate.ad_group_id == init_data['ad_group_id']
    assert browser_aggregate.city == city
    assert browser_aggregate.country == country
    assert browser_aggregate.region == region
    assert browser_aggregate.deviceType == Device.MOBILE.value
    assert browser_aggregate.os == OS.ANDROID.value
    assert browser_aggregate.browser_name == BrowserName.CHROME.value

def test_device_metrics_report(init_data):
    device_aggregate = DeviceMetricAggregate.get_by_adgroup_id(init_data['ad_group_id'], DeviceType.MOBILE.value)

    assert device_aggregate.advertiser_id == init_data['advertiser_id']
    assert device_aggregate.ad_group_id == init_data['ad_group_id']
    assert device_aggregate.device_type == DeviceType.MOBILE.value
    assert device_aggregate.impressions == 3
    assert device_aggregate.click == 0

def test_advertiser_metrics_report(init_data):
    advertiser_aggregate = AdvertiserMetricAggregate.get_by_advertiser_id(init_data['advertiser_id'])

    assert advertiser_aggregate.impressions == init_data['statistic_before'].impressions
    assert advertiser_aggregate.advertiser_id == init_data['advertiser_id']
    assert advertiser_aggregate.click == init_data['statistic_before'].click

def test_ad_group_metrics_report(init_data):
    ad_group_aggregate = AdGroupMetricAggregate.get_by_adgroup_id(init_data['ad_group_id'])

    assert ad_group_aggregate.campaign_group_id == init_data['campaign_group_id']
    assert ad_group_aggregate.advertiser_id == init_data['advertiser_id']
    assert ad_group_aggregate.campaign_id == init_data['campaign_id']
    assert ad_group_aggregate.ad_group_id == init_data['ad_group_id']
    assert ad_group_aggregate.impressions == 3
    assert ad_group_aggregate.click == 1

def test_creatives_metrics_report(init_data):
    creative_aggregate = CreativesMetricAgregate.get_by_creatives_id(init_data['creative_id'])

    assert creative_aggregate.advertiser_id == init_data['advertiser_id']
    assert creative_aggregate.campaign_id == init_data['campaign_id']
    assert creative_aggregate.ad_group_id == init_data['ad_group_id']
    assert creative_aggregate.creative_id == init_data['creative_id']
    assert creative_aggregate.campaign_group_id == init_data['campaign_group_id']
    assert creative_aggregate.impressions == 3
    assert creative_aggregate.clicks == 1