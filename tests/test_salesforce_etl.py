import json

def test_able_to_add_saleforce_data_to_profiles(azkaban_client, hdfs_client, fake):

    file_name = '/dataswitch/import/profile/thirdparty/salesforce/2017-11-11/1.json'

    test_profile_json = json.dumps({
        "AdvertiserId": 3607,
        "Email": "rprokhorov@lineate.com",  # fake.email(),
        "FirstName": "Roman",  # fake.first_name(),
        "Phone": fake.phone_number(),
        "Title": "Dev",
        "LastName": "Prokhorov",  # fake.last_name(),
        "Birthdate": "1947-09-09"
    })

    hdfs_client.write(file_name, test_profile_json, append=True)

    with hdfs_client.read(file_name) as reader:
        content = reader.read()
        print(content.decode('utf-8'))

    #     azkaban_client.execute_flow('Salesforce-import-test', 'profile-import-thirdparty', override_params={
    #     'key': 'date',
    #     'value': '/dataswitch/import/profile/thirdparty/salesforce/2017-11-11'
    # })

    #assert 1 == 1