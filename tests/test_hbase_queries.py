def test_get_list_of_emails_for_a_rule(hbase_client):
    result = hbase_client.fetch_all_users_by_rule_id(20305)

    for key, value in result:
        row = hbase_client.get_row_data(key)
        email = row.get_value_by_column('PII:email')
        print(email)

