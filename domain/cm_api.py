import requests
import string
import random
import json

from collections import namedtuple

def name_generator(size=10, chars=string.ascii_uppercase):
    return ''.join(random.choice(chars) for _ in range(size))


class CMClient:
    def __init__(self, config):
        self.host = config['cm-api']['host']
        self.username = config['cm-api']['username']
        self.password = config['cm-api']['password']
        self.sessionid = self.auth_request().history[0].cookies['JSESSIONID']

    def request(self, method, endpoint='', params=None, json=None):
        response = requests.request(method, 'http://%s/%s' % (self.host, endpoint),
                                    params=params,
                                    json=json,
                                    cookies={
                                        'JSESSIONID': self.sessionid
                                    },
                                    headers={
                                        'content-type': 'application/json'
                                    })
        print(response.url, response.status_code, response.text)
        return response

    def auth_request(self):
        response = requests.request('post', 'http://%s/ds-api/auth/login' % (self.host), data={
            'email': self.username,
            'password': self.password
        })
        print(response.url, response.status_code, response.text)
        return response

    def extract_body_proxy(self, response):
        body = json.loads(response.text)
        return json.loads(json.dumps(body), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

    def __extract_body_cm(self, response):
        body = json.loads(response.text)['body']
        return json.loads(json.dumps(body), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

    def __extract_body_am(self, response):
        return json.loads(response.text)

    def rule_create(self, rule):
        response = self.request('post', 'ase-api/rules/rules', json=rule.__dict__)
        rule.body = self.__extract_body_am(response)
        return response

    def bind_rule_to_segment(self, advertiser_id, segment_id, rule_id):
        return self.request('post',
                            'ase-api/audience/advertiser/%d/segments/%s/rules/%s' % (
                            advertiser_id, segment_id, rule_id))

    def site_create(self, site):
        response = self.request('post', 'ds-api/sites', json=site.__dict__)
        site.body = self.__extract_body_cm(response)
        return response

    def advertiser_create(self, advertiser):
        response = self.request('post', 'ds-api/advertisers', json=advertiser.__dict__)
        advertiser.body = self.__extract_body_cm(response)
        return response

    def advertiser_update(self, id, advertiser):
        response = self.request('put', 'ds-api/advertisers/%d' % id, json=advertiser.__dict__)
        return self.__extract_body_cm(response)

    def campaign_group_create(self, campaign_group):
        response = self.request('post', 'ds-api/campaign-groups', json=campaign_group.__dict__)
        campaign_group.body = self.__extract_body_cm(response)
        return response

    def segment_create(self, advertiser_id, segment):
        response = self.request('post', 'ase-api/audience/advertiser/%d/segments' % (advertiser_id), json=segment.__dict__)
        segment.body = self.__extract_body_am(response)
        return response

    def campaign_create(self, campaign):
        response = self.request('post', 'ds-api/campaigns', json=campaign.__dict__)
        campaign.body = self.__extract_body_cm(response)
        return response

    def adgroup_create(self, adgroup):
        response = self.request('post', 'ds-api/adgroups', json=adgroup.__dict__)
        adgroup.body = self.__extract_body_cm(response)
        return response

    def creative_create(self, creative):
        response = self.request('post', 'ds-api/proxy/creatives', json=creative.__dict__)
        creative.body = self.extract_body_proxy(response)
        return response

    def get_campaign_group(self, campaign_group):
        response = self.request('get', 'ds-api/campaign-groups/%d' % campaign_group)
        return response.text

    def get_campaign(self, campaign):
        response = self.request('get', 'ds-api/campaigns' % campaign)
        return response.text

    def create_deal(self):
        return self.request('post', 'publisher-console/api/deals', json={
            "dealId": name_generator(),
            "description": "Test deal again",
            "endDate": "2017-11-28",
            "name": name_generator(),
            "startDate": "2017-11-27",
            "targeting": {
                "buyers": {15: ["Test"]},
                "devices": {
                    "targetingType": "VALUES",
                    "include": True,
                    "values": [
                        "4",
                        "5"
                    ],
                    "fileId": None
                },
                "geo": {
                    "fileId": None,
                    "include": False,
                    "targetingType": "VALUES",
                    "values": [
                        "new test walue", "Test location"
                    ]
                },
                "products": {
                    "fileId": None,
                    "include": False,
                    "targetingType": "VALUES",
                    "values": [
                        "inview"
                    ]
                },
                "publishers": {
                    "fileId": None,
                    "include": True,
                    "targetingType": "VALUES",
                    "values": [
                        "28"
                    ]
                },
                "segmentId": 11587,
                "sites": {
                    "fileId": None,
                    "include": False,
                    "targetingType": "VALUES",
                    "values": [
                        "http://coffee.com"
                    ]
                },
                "sizes": {
                    "fileId": None,
                    "include": False,
                    "targetingType": "VALUES",
                    "values": [
                        "180x150"
                    ]
                }
            },
            "customTargeting": {
                " test": {
                    "targetingType": "VALUES",
                    "include": False,
                    "values": [
                        "2134"
                    ],
                    "fileId": None,
                    "from": None,
                    "to": None
                },
                "test_demo_1": {
                    "targetingType": "VALUES",
                    "include": False,
                    "values": [
                        "412"
                    ],
                    "fileId": None,
                    "from": None,
                    "to": None
                }
            }
        })

    def clone_campaign(self, from_campaign_group):
        return self.request('post', 'ds-api/campaign-groups/%s/clone' % (from_campaign_group), json={
            'name': name_generator()
        })

    def get_campaign(self, aid, cid):
        return self.request('get',
                            'ds-api/campaigns/deprecated?size=100&direction=desc&properties=id&advertiserId=%s&campaignGroupId=%s&withBindings=false' % (
                                aid, cid))

    def get_creatives(self, gid):
        response = requests.request('post', 'http://%s/ds-api/proxy/creatives' % (self.host), json={
            'stateId': 1,
            'adGroupId': gid,
            'adServerId': 1,
            'vendorId': '1',
            'clickThroughUrl': 'http://test.com',
            'clickTrackingParameter': 'clickTAG',
            'creativeTypeId': 1,
            'name': name_generator(),
            'pacing': 100,
            'creativeAssetId': 2634

        })
        return response

    def create_conversion_path_settings(self, campaign_group_id):
        return self.request('put', 'ds-api/campaign-groups/%s' % (campaign_group_id), json={
            "funnelDuration": 77,
            "assistInteractions": [
                {
                    "actionId": 38475681,
                    "cost": 1
                },
                {
                    "actionId": 38475686,
                    "cost": 2
                }
            ],
            "conversionActions": [
                {
                    "actionId": 38477857
                }
            ]
        })

    def get_conversion_path(self, campaign_group_id):
        return self.request('get',
                            'ds-api/campaign-group-conversion-path-metric-aggregate?'
                            'filters[campaignGroup.id]=%d&page=0&size=25&properties=path&sort=asc'
                            % (campaign_group_id))
