import os
import yaml

def get_config():
    environment = os.getenv('TARGET_ENV')
    with open("config/%s.yml" % (environment), 'r') as ymlfile:
        return yaml.load(ymlfile)