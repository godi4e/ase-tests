import happybase

from retrying import retry
from .profile import Profile

tables = [
  'profiles',
  'segment_core'
]

table_names = ["profiles", "segment_core"]

class HBaseClient:
    def __init__(self, config):
        self.connection = happybase.Connection(config['hbase']['host'], config['hbase']['port'])
        self.table = self.connection.table(config['hbase']['profiles-table'])

    # def init_tables(self):
    #     for table_name in table_names:
    #         print("Creating table", table_name)
    #         self.connection.create_table(
    #             table_name,
    #             families={
    #                 'HISTORY': dict(max_versions=1000, bloom_filter_type=None, block_cache_enabled=False),
    #                 'PII': dict(max_versions=1000, bloom_filter_type=None, block_cache_enabled=False),
    #                 'SEGMENT': dict(max_versions=1, bloom_filter_type=None, block_cache_enabled=False),
    #                 'INFO': dict(max_versions=1, bloom_filter_type=None, block_cache_enabled=False)
    #             }
    #         )
    #
    # def delete_all_tables(self):
    #     for table_name in table_names:
    #         print("Removing table", table_name)
    #         self.connection.delete_table(table_name, True)

    @retry(wait_fixed=2000,stop_max_delay=30000)
    def _scan_with_filter(self, filter):
        self.table.scan(filter=filter)
        for key, data in self.table.scan(filter=filter):
            return key

        print('.')
        raise IOError("Event hasn't been delivered to HBase within timeout. Seems like exporter-job isn't working at the moment")

    def get_row_key_by_user_id(self, user_id):
        filter = "ValueFilter(= , 'binaryprefix:%s')" % user_id
        return self._scan_with_filter(filter)

    def fetch_all_users_by_rule_id(self, rule_id):
        filter = "ColumnPrefixFilter('ds/%d')" % rule_id
        return self.table.scan(filter=filter)

    def print_all_data(self):
        try:
            for key, data in self.table.scan():
                print(data)
        except:
            print("Can't display data from the table")

    def get_row_data(self, row_key):
        row_data = self.table.row(row_key)

        return Profile(row_data)