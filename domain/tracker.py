import http.cookies
import requests

from urllib import request

class Response:
    def __init__(self, response, tracker):
        self.response = response
        self.tracker = tracker
        cookies = http.cookies.SimpleCookie()

        if response.history:
            for r in response.history:
                print('url: ', r.url)
                if self.tracker.host in r.url:
                    cookies.load(r.headers['Set-Cookie'])
                    self.user_id = cookies["dswuserid"].value
                    print(r.url)
        else:
            cookies.load(response.headers['Set-Cookie'])
            self.user_id = cookies["dswuserid"].value
            print(response.url)
        print('dswuserid ', self.user_id)


class Tracker:
    def __init__(self, config):
        # self.protocol = config['protocol']
        # self.host = config['host']
        # self.endpoints = config['endpoints']
        # self.params = config['params']
        self.nodes = config['tracker']['nodes']

    def get_endpoint(self, endpoint):
        return self.protocol + '://' + self.host + self.endpoints[endpoint]

    def rollover(self):
        opener = request.build_opener()
        for node in self.nodes:
            opener.open(node)

class Event:
    def __init__(self, params):
        self.type = []
        self.params = params

    def send_to(self, tracker):
        tracker_endpoint = tracker.get_endpoint('event')
        headers = {
            'User-Agent': 'My User Agent 1.0'
        }
        response = requests.get(tracker_endpoint, self.params, headers=headers)

        return Response(response, tracker)