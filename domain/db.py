import pymssql

class DBConnection():
    def get_connection(self):
        conn = pymssql.connect(self.host, self.user, self.password, self.db)
        return conn.cursor()

class CMDBConnection(DBConnection):
    def __init__(self, config):
        self.host = config['cm-db']['host']
        self.user = config['cm-db']['user']
        self.password = config['cm-db']['password']
        self.db = config['cm-db']['db']

class SSPDBConnection(DBConnection):
    def __init__(self, config):
        self.host = config['ssp-db']['host']
        self.user = config['ssp-db']['user']
        self.password = config['ssp-db']['password']
        self.db = config['ssp-db']['db']

