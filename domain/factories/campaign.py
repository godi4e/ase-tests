import random
import datetime

start_date = datetime.datetime.now()
end_date = start_date + datetime.timedelta(days=7)

class SimplifiCampaign():
    def __init__(self, advertiser_id, campaign_group_name):
        self.name = '__pytest-campaign-%d' % random.randint(1000, 1000000)
        self.campaignGroupName = campaign_group_name
        self.startDate = start_date.strftime("%Y-%m-%d")
        self.endDate = end_date.strftime("%Y-%m-%d")
        self.dailyImpressionCamp = 100
        self.isSpendPrioritizationEnables = True
        self.lifetimeBudget = 1000
        self.lifetimeImpressionsCap = 1000000
        self.vendorId = 1
        self.advertiserId = advertiser_id
