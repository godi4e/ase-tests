import random

class Site():
    def __init__(self, advertiser_id, urls):
        self.name = '__pytest-site-%d' % random.randint(1000, 1000000)
        self.advertiserId = advertiser_id
        self.urls = [urls]