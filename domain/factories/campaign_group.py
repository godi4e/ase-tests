import random
import datetime

start_date = datetime.datetime.now()
end_date = start_date + datetime.timedelta(days=7)


class CampaignGroup():
    def __init__(self, advertiser_id):
        self.name = '__pytest-campaign-group-%d' % random.randint(1000, 1000000)
        self.startDate = start_date.strftime("%Y-%m-%d")
        self.endDate = end_date.strftime("%Y-%m-%d")
        self.advertiserId = advertiser_id
        self.lifetimeBudget = 10000
        self.lifetimeImpressionsCap = 3000000
