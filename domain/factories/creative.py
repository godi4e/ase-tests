import random


class SimplifiCreative():
    def __init__(self, adgroup_id):
        self.name = '__pytest-creative-%d' % random.randint(1000, 1000000)
        self.stateId = 1
        self.adGroupId = adgroup_id
        self.adServerId = 1
        self.vendorId = '1'
        self.clickThroughUrl = 'http://test.com'
        self.clickTrackingParameter = 'clickTAG'
        self.creativeTypeId = 1
        self.pacing = 100
        self.creativeAssetId = 2790