import random
import datetime

start_date = datetime.datetime.now()
end_date = start_date + datetime.timedelta(days=7)

class SimplifiGroup():
    def __init__(self, campaign_id):
        self.name = '__pytest-adgroup-%d' % random.randint(1000, 1000000)
        self.campaignId = campaign_id
        self.startDate = start_date.strftime("%Y-%m-%d")
        self.endDate = end_date.strftime("%Y-%m-%d")
        self.lifetimeSpend = 1
        self.maxBid = 10
        self.frequencyPricingSlope = 1
        self.autoOptimize = True
        self.baseBid = 5
        self.baseBidAutoOptimization = True
        self.dailySpend =100
        self.vendorClassificationId = 1
        self.biddingValue =10

