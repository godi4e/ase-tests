import random

class Advertiser():
    def __init__(self):
        self.name = '__pytest-advertiser-%d' % random.randint(1000, 1000000)
        self.crmId = random.randint(1000, 100000)
        self.applicationId = random.randint(1000, 100000)

class AdvertiserTradeDeskSettings():
    def __init__(self):
        self.clickAttributionWindow = 90
        self.clickDeDuplicationWindow = 7
        self.clickFrequencyId = 3
        self.conversionDeDuplicationWindow = 60
        self.domainAddress = 'http://coffee.com'
        self.logoUrl = 'http://bipbap.ru/wp-content/uploads/2017/04/000f_7290754.jpg'
        self.impressionAttributionWindow = 60
        self.impressionsFrequencyId = 1
        self.industryCategoryId = 5
        self.industrySubCategoryId = 58
        self.yahooCategoryId = 3

class AdvertiserCouponSettings():
    def __init__(self):
        self.googleAnalyticsKey = random.randint(10000000, 99999999)

class AdvertiserMailChimpSettings():
    def __init__(self):
        self.mailChimp = {
            'apiKey': 'eba9414ac34fede2457df39c0c1d5bc5-us16',
            'companyAddress': 'Gukova 21',
            'companyName': 'Thumbtack',
            'emailFrom': 'test@example.com'
        }