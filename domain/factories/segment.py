import random

class Segment():
    def __init__(self, advertiser_id):
        self.name = '__pytest-segment-%d' % random.randint(1000, 1000000)
        self.rules = []
        self.categoryId = None
        self.categoryName = None
        self.associationId = None
        self.productLineId = None
        self.projectId = None
        self.ownerId = None
        self.source = "MV"
        self.usersCount = None
        self.ttl = 7776000000
        self.sourceId = "5"
        self.supportedChannels = ["TradeDesk", "Direct"]
        self.ruleValue = None
        self.anonymous = True
        self.type = "advertiser"
        self.advertiserId = advertiser_id
        self._categories = [{"id": "2131", "name": "asdasd"}]
        self.categories = None
        self.description = "{\"sourceId\":\"5\",\"ruleValue\":null,\"supportedChannels\":[\"TradeDesk\",\"Direct\"],\"anonymous\":true} "