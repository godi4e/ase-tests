import json

from .. import cm_api
from ..config_init import get_config

c = get_config()
client = cm_api.CMClient(c)
class CampaignGroup:
    def __init__(self, from_campaign_group):
        self.data = json.loads(client.clone_campaign(from_campaign_group=from_campaign_group).text)

    def set_conversion_settings(self):
        response = client.create_conversion_path_settings(self.data['body']['id'])
        return response

    def get_campaign_group_id(self):
        response = self.data['body']['id']
        return response

    def get_advertiserId(self):
        response = self.data['body']['advertiserId']
        return response

    def get_conversion_report(self, campaign_group_id):
        response = client.read_path(campaign_group_id)
        js = json.loads(response.text)
        return js['content']