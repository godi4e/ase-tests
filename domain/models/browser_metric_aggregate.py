from ..db import CMDBConnection
from ..config_init import get_config

c = get_config()


class BrowserMetricAggregateObject():
    def __init__(self, row):
        self.event_type = row[3]
        self.action_id = int(row[4])
        self.os = row[22]
        self.deviceType = int(row[29])
        self.country = row[25]
        self.region = row[27]
        self.city = row[26]
        self.zip = row[24]
        self.advertiser_id = int(row[40])
        self.campaign_id = int(row[42])
        self.ad_group_id = int(row[45])
        self.creative_id = int(row[46])
        self.browser_name = row[54]
        self.browser_version = int(row[55])

class BrowserMetricAggregate():
    @staticmethod
    def get_by_adgroup_id(gid, event_type):
        db = CMDBConnection(c)
        cursor = db.get_connection()
        cursor.execute("SELECT * FROM dbo.DW_AGGREGATE_HOUR WHERE adGroupId=%d and eventType=%d", (gid, int(event_type)))
        result = cursor.fetchone()
        return BrowserMetricAggregateObject(result)
