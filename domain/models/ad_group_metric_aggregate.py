from ..db import CMDBConnection
from ..config_init import get_config

c = get_config()

class AdGroupMetricAggregateObject():
    def __init__(self, row):
        self.advertiser_id = row[0]
        self.campaign_id = row[1]
        self.ad_group_id = row[2]
        self.impressions = row[4]
        self.click = row[5]
        self.created_date_time = row[12]
        self.campaign_group_id = row[14]

class AdGroupMetricAggregate():
    @staticmethod
    def get_by_adgroup_id(gid):
        db = CMDBConnection(c)
        cursor = db.get_connection()
        cursor.execute("SELECT * FROM dbo.MV_AdGroupMetricsAggregates WHERE AdGroupId=%d", gid)
        result = cursor.fetchone()
        return AdGroupMetricAggregateObject(result)
