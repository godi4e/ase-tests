import pymssql
import json
from retrying import retry

from .. import cm_api

server = 'aa-cm-sql-dev.thumbtack.lo'
cm_user = 'ttdev'
password = 'kaiK1dosh3tai2oogae0'
cm_db = 'MALT_DEV'
client = cm_api.CMClient('x')


class ReportsDBObject():
    def __init__(self, deal):
        self.id = deal[0]
        self.name = deal[1]
        self.devices = deal[10]
        self.deal_id = deal[3]
        self.bidders = deal[5]
        self.publishers = deal[6]
        self.landing_page = deal[7]
        self.geo = deal[8]
        self.key_value = deal[9]
        self.size = deal[11]
        self.start_date = deal[13].isoformat()
        self.end_date = deal[14].isoformat()
        self.segments_include = deal[21]

class Deal:
    def __init__(self):
        self.data = json.loads(client.create_deal().text)
        print(self.data)

    @retry(wait_fixed=2000, stop_max_delay=30000)
    def get_deal_from_cm_db(self):
        conn = pymssql.connect(server, cm_user, password, cm_db)
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM dbo.deal WHERE Id=%d", self.data['id'])
        result = cursor.fetchone()

        if result:
            return ReportsDBObject(result)

        raise ("The sync to ssp db wasn't executed within specified timeout")
