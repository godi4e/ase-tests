import json
from .. import cm_api
from ..config_init import get_config
from ..db import CMDBConnection

c = get_config()
client = cm_api.CMClient(c)

class Creatives:
    def __init__(self, gid):
        self.data = json.loads(client.get_creatives(gid).text)
        print("created creative: ", self.data)

    def get_creative_id(self):
        response = self.data['id']
        return response


class CreativesMetricAgregate():
    def __init__(self, row):
        self.advertiser_id = row[0]
        self.campaign_id = row[1]
        self.ad_group_id = row[2]
        self.creative_id = row[3]
        self.impressions = row[5]
        self.clicks = row[6]
        self.campaign_group_id = row[15]

    @staticmethod
    def get_by_creatives_id(creative_id):
        db = CMDBConnection(c)
        cursor = db.get_connection()
        cursor.execute("SELECT * FROM dbo.MV_CreativeMetricsAggregates WHERE CreativeId=%d", creative_id)
        result = cursor.fetchone()
        return CreativesMetricAgregate(result)
