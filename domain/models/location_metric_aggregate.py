from ..db import CMDBConnection
from ..config_init import get_config

c = get_config()

class LocationMetricAggregateObject():
    def __init__(self, row):
        self.advertiser_id = row[1]
        self.ad_group_id = row[2]
        self.country = row[5]
        self.region = row[6]
        self.city = row[8]
        self.impressions = row[9]
        self.clicks = row[10]

class LocationMetricAggregate():
    @staticmethod
    def get_by_adgroup_id(gid):
        db = CMDBConnection(c)
        cursor = db.get_connection()
        cursor.execute("SELECT * FROM dbo.MV_LocationMetricsAggregates WHERE AdGroupId=%d", gid)
        result = cursor.fetchone()
        return LocationMetricAggregateObject(result)
