import json
from .. import cm_api
from ..config_init import get_config

c = get_config()
client = cm_api.CMClient(c)

class Campaign:
    def __init__(self, aid, cid):
        self.data = json.loads(client.get_campaign(aid, cid).text)

    def get_campaign_id(self):
        response = self.data['body']['content'][0]['id']
        return response

    def get_group_id(self):
        response = self.data['body']['content'][0]['adGroups'][0]['id']
        return response