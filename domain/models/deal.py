import json
from ..config_init import get_config

from datetime import datetime
from retrying import retry

from ..cm_api import CMClient
from ..db import CMDBConnection
from ..db import SSPDBConnection

c = get_config()

class DealDBObject():
    def __init__(self, deal_id):
        deal = self._get_deal_by_id(deal_id)
        self.id = deal[0]
        self.name = deal[1]
        self.devices = deal[10]
        self.deal_id = deal[3]
        self.bidders = deal[5]
        self.publishers = deal[6]
        self.landing_page = deal[7]
        self.geo = deal[8]
        self.key_value = deal[9]
        self.size = deal[11]
        self.start_date = deal[13].isoformat()
        self.end_date = deal[14].isoformat()
        self.segments_include = deal[21]

    def _get_deal_by_id(self, deal_id):
        db = CMDBConnection(c)
        cursor = db.get_connection()
        cursor.execute("SELECT * FROM dbo.deal WHERE Id=%d", deal_id)
        return cursor.fetchone()

class DealSSPObject():
    def __init__(self, deal_id):
        deal = self._get_deal_by_id(deal_id)
        self.id = deal[0]
        self.name = deal[16]
        self.devices = deal[17]
        self.bidders = deal[10]
        self.publishers = deal[13]
        self.landing_page = deal[12]
        self.geo = deal[11]
        self.key_value = deal[18]
        self.size = deal[19]
        self.start_date = datetime.strptime(deal[9][:-1], "%Y-%m-%d %H:%M:%S.%f").isoformat()
        self.end_date = datetime.strptime(deal[3][:-1], "%Y-%m-%d %H:%M:%S.%f").isoformat()
        self.segments_include = deal[23]

    @retry(wait_fixed=2000, stop_max_delay=12000)
    def _get_deal_by_id(self, deal_id):
        db = SSPDBConnection(c)
        cursor = db.get_connection()
        cursor.execute("SELECT * FROM dbo.deal_v2 WHERE Id=%d", deal_id)
        result = cursor.fetchone()
        if result:
            return result
        raise ("The sync to ssp db wasn't executed within specified timeout")

class Deal:
    def __init__(self):
        client = CMClient(c)
        self.data = json.loads(client.create_deal().text)

    def get_deal_from_cm_db(self):
        return DealDBObject(self.data['id'])

    def get_deal_from_ssp_db(self):
        return DealSSPObject(self.data['dealId'])
