from ..db import CMDBConnection
from ..config_init import get_config

c = get_config()


class AdvertiserMetricAggregateObject():
    def __init__(self, row):
        self.advertiser_id = row[0]
        self.impressions = row[2]
        self.click = row[3]
        self.created_date_time = row[10]

class AdvertiserMetricAggregate():
    @staticmethod
    def get_by_advertiser_id(aid):
        db = CMDBConnection(c)
        cursor = db.get_connection()
        cursor.execute("SELECT * FROM dbo.MV_AdvertiserMetricsAggregates WHERE AdvertiserId=%d ORDER BY Date DESC", (aid))
        result = cursor.fetchone()
        return AdvertiserMetricAggregateObject(result)
