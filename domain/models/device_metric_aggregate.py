from ..db import CMDBConnection
from ..config_init import get_config

c = get_config()


class DeviceMetricAggregateObject():
    def __init__(self, row):
        self.advertiser_id = row[1]
        self.ad_group_id = row[2]
        self.device_type = row[4]
        self.impressions = row[5]
        self.click = row[6]
        self.date = row[8]

class DeviceMetricAggregate():
    @staticmethod
    def get_by_adgroup_id(gid, device_type):
        db = CMDBConnection(c)
        cursor = db.get_connection()
        cursor.execute("SELECT * FROM dbo.MV_DeviceMetricsAggregates WHERE AdGroupId=%d and DeviceType=%s", (gid, device_type))
        result = cursor.fetchone()
        return DeviceMetricAggregateObject(result)
