from urllib import parse
from urllib import request

import datetime
import os
import yaml

with open(os.path.join(os.path.dirname(__file__), "../config/%s.yml" % (os.getenv('TARGET_ENV'))), 'r') as ymlfile:
    config = yaml.load(ymlfile)
    base_url = "%s://%s:%d%s" % (config['tracker']['protocol'],
                                 config['tracker']['host'],
                                 config['tracker']['port'],
                                 config['tracker']['event'])
    print('test baseurl', base_url)


class LogRecord:
    def __init__(self, csv_line):
        values = csv_line.split(',')
        self.user = values[5]
        self.campaign_id = int(values[6])
        self.action_id = int(values[8])
        self.days = days_before_today_to_timestamp(int(values[9]))


def days_before_today_to_timestamp(days):
    if days:
        now_time = datetime.datetime.now()
        delta = datetime.timedelta(days=days)
        now_date = now_time - delta
        return int(now_date.timestamp() * 1000)


def transform_csv_line_to_tracking_url(params, headers):
    opener = request.build_opener()
    opener.addheaders = headers

    res = {k: v for k, v in params.items() if v is not None}

    queryparams = parse.urlencode(res)

    url = '%s?%s' % (base_url, queryparams)
    print('URL: %s' % (url,))

    opener.open(url)

def from_csv(filename):
    with open(filename) as csvfile:
        next(csvfile)
        for row in csvfile:
            log_record = LogRecord(row)
            transform_csv_line_to_tracking_url(row)
