from domain.generator import transform_csv_line_to_tracking_url, days_before_today_to_timestamp


class User:
    def __init__(self):
        self.user_id = 'userBeh2'

    def send_event_to_tracker(self,
                              event_type_id,
                              type=None,
                              country=None,
                              zip=None,
                              advertiser_id=None,
                              group_id=None,
                              creative_id=None,
                              city=None,
                              region=None,
                              campaign_id=None,
                              days_ago=None,
                              user_agent=None,
                              location=None,
                              landing_page=None,
                              referer=None):
        params = {
            'timestamp': days_before_today_to_timestamp(days_ago),
            'event_type_id': event_type_id,
            'campaignId': campaign_id,
            'userId': self.user_id,
            'landingPage': landing_page,
            'user-agent': user_agent,
            'ip_address': location,
            'advertiserId': advertiser_id,
            'ad_group_Id': group_id,
            'creativeId': creative_id,
            'city': city,
            'region': region,
            'country': country,
            'zip': zip,
            'action_id': type,
            'referer': referer
        }

        headers = [('Cookie', 'dswuserid=%s' % self.user_id), ('Referer', '' if not referer else referer)]

        transform_csv_line_to_tracking_url(params, headers)
