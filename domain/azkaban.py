import requests
import json

from retrying import retry


class AzkabanClient:
    def __init__(self, config):
        self.host = config['azkaban']['host']
        self.port = config['azkaban']['port']
        self.protocol = config['azkaban']['protocol']
        self.username = config['azkaban']['user']
        self.password = config['azkaban']['password']
        self.sessionid = json.loads(self.auth_request().text)['session.id']

    def request(self, method, endpoint='', data=None, params=None):
        return requests.request(method, '%s://%s:%d/%s' % (self.protocol, self.host, self.port, endpoint), data=data, params=params,
                                verify=False)

    def auth_request(self):
        return self.request('post', data={
            'action': 'login',
            'username': self.username,
            'password': self.password
        })

    @retry(wait_fixed=2000, stop_max_delay=900000)
    def __get_flow_status(self, exec_id):
        response = self.request('get', 'executor', params={
            'session.id': self.sessionid,
            'ajax': 'fetchexecflow',
            'execid': exec_id
        })
        js = json.loads(response.text)

        print(js['status'])

        if js['status'] == 'SUCCEEDED':
            return

        raise ("The job hasn't finished within specified timeout")

    def execute_flow(self, project: object, flow: object) -> object:
        response = self.request('get', 'executor', params={
            'session.id': self.sessionid,
            'ajax': 'executeFlow',
            'project': project,
            'flow': flow
        })
        id = json.loads(response.text)

        return self.__get_flow_status(id['execid'])
