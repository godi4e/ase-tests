import json

segment_qualifier_prefix = 'SEGMENT:ds/'
email_qualifier_prefix = 'PII:email'
advertiser_qualifier_prefix = 'HISTORY:advertiserId'

class Profile:
    def __init__(self, row_data):
        self.row_data = row_data

    def __to_json(self, bytes):
        return json.loads(bytes.decode('utf-8'))

    def get_segment_data_by_id(self, rule_id):
        row_name = (segment_qualifier_prefix + rule_id).encode('utf-8')
        return self.__to_json(self.row_data[row_name])

    def get_email(self):
        return self.row_data[email_qualifier_prefix.encode('utf-8')].decode("utf-8")

    def get_advertiser_id(self):
        return self.row_data[advertiser_qualifier_prefix.encode('utf-8')].decode("utf-8")

    def get_value_by_column(self, column_name):
        return self.row_data[column_name.encode('utf-8')].decode("utf-8")


